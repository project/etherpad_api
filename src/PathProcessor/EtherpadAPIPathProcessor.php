<?php

namespace Drupal\etherpad_api\PathProcessor;

use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Symfony\Component\HttpFoundation\Request;

/**
 * Path processor to replace 'node' with 'content' in URLs.
 */
class EtherpadAPIPathProcessor implements InboundPathProcessorInterface {
  const PREFIX = '/etherpad-api/';

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request) {
    if (strpos($path, self::PREFIX) === 0) {
      $subpath = substr($path, strlen(self::PREFIX));
      return '/etherpad-api/' . str_replace('/', ':', $subpath);
    }
    return $path;
  }
}
