<?php

namespace Drupal\etherpad_api\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use GuzzleHttp\Exception\ClientException;

use Drupal\etherpad_api\Client;

/**
 * Returns responses for Etherpad API routes.
 */
class EtherpadApiController extends ControllerBase {
  protected $client;

  public function __construct(Client $client) {
    $this->client = $client;
  }

  public static function create(ContainerInterface $container) {
    return new static($container->get('etherpad_api.client'));
  }

  /**
   * Builds the response.
   */
  public function build($components, Request $request) {
    $uri = str_replace(':', '/', $components);
    if ($params = $request->getQueryString()) {
      $uri .= "?$params";
    }

    $data = array_merge($request->query->all(), $request->request->all());
    if ($data['padID']) {
      // @TODO prefix v config!
      // Zahtevnejše ampak lepše bi bilo pa uporabit group API:
      // https://etherpad.org/doc/v1.8.4/#index_creategroup
      $data['padID'] = 'yufu-' . $data['padID'];
    }
    $opts = ['form_params' => $data];

    try {
      return $this->client->request($uri, $opts);
    } catch (ClientException $exception) {
      return new Response($exception->getMessage(), $exception->getCode());
    }
  }
}
