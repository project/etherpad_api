# Etherpad API

This module can be used to proxy requests to an etherpad API.
After installing the module, you must set the etherpad instance API URL and the API key.

Etherpad API documentation can be found here: https://etherpad.org/doc/v1.8.4/#index_http_api

The etherpad API proxy URL is /etherpad-api (@TODO make configurable). When calling it, you can omit the version part (e.g. /1) and the API key (which is set in the configuration). The pad ID prefix is hardcoded for the moment (so that requests are limited to specific pads, not the whole instance).

Some example API calls can found in [api.rest](doc/api.rest).

You can do either GET or POST requests to the proxy, passing parameters via query or post parameters. POST parameters must be passed as key-value pairs (application/x-www-form-urlencoded). The proxy always makes POST requests to etherpad, for the sake of simplicity. The API key is always added, the padID is always prefixed.

## TODO

- configurable PAD ID prefix
- configurable proxy API path
- API call whitelist
- security audit
- use pad groups instead of prefixed pads? (https://etherpad.org/doc/v1.8.4/#index_groups)
